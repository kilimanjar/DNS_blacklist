#!/bin/bash

BLACKLISTFILE="./unbound/blacklist.rpz"

{
    wget -q 'http://malwaredomains.lehigh.edu/files/immortal_domains.txt' -O - | grep -v \#; \
    wget -q 'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=1&startdate%5Bday%5D=&startdate%5Bmonth%5D=&startdate%5Byear%5D=&mimetype=plaintext' -O - ; \
    wget -q 'http://malwaredomains.lehigh.edu/files/BOOT' -O - | grep PRIMARY | cut -d " " -f 2; \
    wget -q 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts' -O - | grep "^0" | cut -d " " -f 2; \
} | sort -u | sed -e 's/\(^.*$\)/local-zone: \"\1\" redirect\nlocal-data: \"\1 A 127\.0\.0\.1\"/' >> ${BLACKLISTFILE}


exit 0
