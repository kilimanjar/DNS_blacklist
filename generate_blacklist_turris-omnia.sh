#!/bin/bash

BLACKLISTFILE="/etc/kresd/blacklist.rpz"

cat > ${BLACKLISTFILE} <<EOF
\$TTL 60
@               IN      SOA  localhost. root.localhost.  (
            2   ; serial
            3H  ; refresh
            1H  ; retry
            1W  ; expiry
            1H) ; minimum
        IN      NS    localhost.
EOF

{
    wget -q 'http://malwaredomains.lehigh.edu/files/immortal_domains.txt' -O - | grep -v \#; \
    wget -q 'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=1&startdate%5Bday%5D=&startdate%5Bmonth%5D=&startdate%5Byear%5D=&mimetype=plaintext' -O - ; \
    wget -q 'http://malwaredomains.lehigh.edu/files/BOOT' -O - | grep PRIMARY | cut -d " " -f 2; \
    wget -q 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts' -O - | grep "^0" | cut -d " " -f 2; \
} | sort -u | sed -e 's/$/\tCNAME\t./' >> ${BLACKLISTFILE}

/etc/init.d/kresd reload

exit 0
